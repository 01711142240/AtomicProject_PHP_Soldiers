<?php

namespace App\Bitm\PHP_Soldiers\Date;
use App\BITM\PHP_Soldiers\Utility\Utility;

class Birthday {

    public $table = "birthday";
    public $id;
    public $name;
    public $date;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    public $connection;

    public function __construct($data = false) {
        $this->name = $data['name'];
        $this->date = $data['date'];
        $this->id = $data['id'];
        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("atomicproject", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $hobbys = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY id DESC";
        // SELECT column_name,column_name FROM table_name ORDER BY column_name,column_name ASC|DESC;
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $hobbys[] = $row;
        }
        return $hobbys;
    }

    public function store() {

        //$query = "INSERT INTO ". $this->table ." ( `name`,`date`) VALUES ( '" . $this->name . "','" . $this->date . "')";
        $query = "INSERT INTO `" . $this->table . "` (`name`, `date`) VALUES ('" . $this->name . "', '" . $this->date . "')";
        $result = mysql_query($query);

        if ($result) {
            $message = "<h2>Birthday is added successfully.</h2>";
            Utility::message($message);
            header("Location: index.php");
        } else {
            $message = "<h2>Birthday  is not added successfully.</h2>";
            Utility::message($message);
        }
        
    }

    public function get_single_id_details($id) {

        $query = "SELECT * FROM " . $this->table . " WHERE id='$id' ";
        $result = mysql_query($query);

        return $result;
    }

    public function update_single_id_details() {

        $query = "UPDATE `atomicproject`.`" . $this->table . "` SET `name` = '" . $this->name . "', `date` = '" . $this->date . "' WHERE `birthday`.`id` = " . $this->id . "";

        if (mysql_query($query)) {
            header('Location:index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function deleted_single_id_forever($id) {

        $query = "DELETE FROM " . $this->table . " WHERE `" . $this->table . "`.`id` = '$id'";

        if (mysql_query($query)) {
            header('Location:index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function create() {
        echo 'I am create form';
    }

    public function edit() {
        echo 'I am editing data';
    }

    public function update() {
        echo 'I am updateing data';
    }

}
