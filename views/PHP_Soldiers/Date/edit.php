<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
require_once(SITE_ROOT . DS . "vendor/autoload.php");

use App\Bitm\PHP_Soldiers\Date\Birthday;

$birthday = new Birthday();

//$id = $_GET['id'];
$single_id = $birthday->get_single_id_details($_GET['id']);
$single = mysql_fetch_assoc($single_id);
//print_r($single);
//exit();
//if (isset($_POST['submit'])) {
//
//    $birthday = new Birthday($_POST);
//
//    $result = $birthday->update_single_id_details();
//}

?>  
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="list.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Birthday</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="update.php" method="POST" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="bookmark">Edit Person Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="bookmark" 
                                       type="text" 
                                       value="<?php echo $single['name']; ?>"
                                       name="name"
                                       tabindex="1"
                                       required="required" 
                                       >
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="bookmark">Edit Date of Birth</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="bookmark" 
                                       type="text" 
                                       value="<?php echo $single['date']; ?>"
                                       name="date"
                                       tabindex="2"
                                       required="required" 
                                       >
                            </div>
                        </div> 
                        <div class="control-group">
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       id="hobby"
                                       type="hidden" 
                                       value="<?php echo $single['id']; ?>"
                                       name="id"
                                       >
                            </div>
                        </div> 
                        <div class="form-actions">
                            <button type="submit" tabindex="3" class="btn btn-primary">Save</button>
                            <button type="submit" tabindex="4" class="btn btn-primary">Save & Add Again</button>
                            <input tabindex="5" class="btn" type="reset" value="Reset" />
                        </div>
                    </fieldset>
                </form>  

            </div>
        </div><!--/span-->
        <h1><?php //$birthday->edit();   ?></h1>
    </div><!--/row-->
    <a class="btn btn-success" href="index.php">Go to List</a>
    <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>





</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>